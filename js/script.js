var UIController = function() {
    return {
        setUpUI: function() {
            //Dropdown menu
            $(document).ready(function(){
                var browserWidth = window.innerWidth;
                if(browserWidth >= 1200){
                    $(".my-dropdown").hover(function() {
                        $(this).children(".my-dropdown-content").stop(true, false, true).slideToggle(350);
                    });
                }
            });
        }
    }
}();
var controller = function(UICtrl) {
    return {
        init: function() {
            UICtrl.setUpUI();
        }
    }
}(UIController);

controller.init();